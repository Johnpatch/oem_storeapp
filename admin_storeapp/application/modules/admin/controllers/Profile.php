<?php

/*
 * @Author:    Kiril Kirkov
 *  Gitgub:    https://github.com/kirilkirkov
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Profile extends ADMIN_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Admin_Model');
        $this->load->model('Oem_Model');
	$this->load->model("Setting_Model");
    }

    public function index($page = 0)
    {
     /*   $user = $this->session->userdata('user');
        if (!$user) {
            redirect('admin/login');
        } */
        $user = $this->get_session_data($this->token , 'user');
        $user = json_encode($user);
        $user = json_decode($user);
        $data = $this->head;
        $data["title"] = lang("profile");
	$setting = $this->Setting_Model->one();

        $this->load->view('_parts/header', $data);
        $this->load->view('profile', array('token' => $this->token , 'user' => $user, 'setting'=>$setting));
        $this->load->view('_parts/footer');
    }

    public function submit() {
        $data = $_POST;
        $type = $data['TYPE'];  unset($data['TYPE']);
        unset($data['LOGO']);
	unset($data['pdf_file']);
        $data['PASSWORD'] = md5($data['PASSWORD']);
        if ($type == 'admin')
            $this->Admin_Model->save($data);
        else
            $this->Oem_Model->save($data);

        $data = $_POST;
        unset($data['PASSWORD']);
        unset($data['TYPE']);
        if ($data["LOGO"]) {
            $this->session->set_userdata('logo', $data["LOGO"]);
        } else {
            unset($data["LOGO"]);
        }
	$config['upload_path']          = './uploads/';
        $config['allowed_types'] = 'pdf';
        $config['file_name'] = random_string().".pdf";
        $this->load->library('upload', $config);
        $this->upload->do_upload('pdf_file');
	$data['MANUAL'] = $config['file_name'];
        $result = $this->db->where('ID',$data['ID'])->get('basic_setting')->row_array();
        if(count($result) > 0){
            $this->db->where('ID', $data['ID']);
            $this->db->update('basic_setting',$data);
        }else{
            $this->db->insert('basic_setting', $data);
        }
//        $this->Setting_Model->save($data);
        redirect('admin/profile'.'?token='.$this->token);
    }

    public function checkExist() {
        $result["exist"] = "false";
        $id = $_POST['id'];
        $already_admin = $this->Admin_Model->one(["LOGIN_ID" => $id]);
        if ($already_admin) {
            $result["exist"] ="true";
        } else {
            $already_oem = $this->Oem_Model->one(["LOGIN_ID" => $id]);
            if ($already_oem) $result["exist"] ="true";
        }
        print_r(json_encode($result));

    }
}
