<?php

/*
 * @Author:    Kiril Kirkov
 *  Gitgub:    https://github.com/kirilkirkov
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Headerfooter extends ADMIN_Controller
{
    protected $table = 'header_footer_settings';
    protected $route_prefix = 'admin/design/headerfooter/';//Dont't forget to attach '/' to the end of the string
    protected $has_index = false;//true if this controller has list pages
//    protected $page_title = "Design setting";
//    protected $page_subtitle = "header footer image / edit";
//    protected $panel_title = "Header/footer image settings";
    protected $page_icon = "<i class='fa fa-sitemap'></i>";
    /*
     * Input Fields Description
     * name: input name----Must be exactly equal to corresponding DB column name
     * type: input type [text|file|checkbox|radio|textarea|static|dropdown|color|...]
     * default: default value if initial value is not set
     * label: input label description
     * help: help block description
     * required: whether this field is requried or not
     * showIf: array(key=>value). shows this element when (key=value)
     *          value can take array values.
     * rules: validation settings
     */
//    protected $fields = [
//        [
//            "group"=>true,
//            "label"=>"Header image(jpeg,jpg,gif,png)",
//            "input"=>[[
//                "name"=>"HEADER_IMAGE",
//                "type"=>"file",
//                "help"=>"<span class='help-tab-gray'>Format</span> jpeg,jpg,gif,png</br>
//                     <span class='help-tab-gray'>Recommended size</span>640*84</br>
//                     <span class='help-tab-gray'>capacity</span>Please register the image within 300KB</br>
//                     <span class='help-tab-gray'>File name</span>Alphanumeric file name
//                     ",
//            ],
////            [
////                "name"=>"HEADER_IMAGE_DEL_YN",
////                "type"=>"checkbox",
////                "default"=>"Y",
////                "text"=>"Delete"
////            ]
//                ],
//        ],
//        [
//            "group"=>true,
//            "label"=>"Footer image(jpeg,jpg,gif,png)",
//            "input"=>[[
//                "name"=>"FOOTER_IMAGE",
//                "type"=>"file",
//                "help"=>"<span class='help-tab-gray'>Format</span> jpeg,jpg,gif,png</br>
//                     <span class='help-tab-gray'>Recommended size</span>640*100</br>
//                     <span class='help-tab-gray'>capacity</span>Please register the image of less than 300KB</br>
//                     <span class='help-tab-gray'>File name</span>Alphanumeric characters in the file name
//                     ",
//            ],
////            [
////                "name"=>"FOOTER_IMAGE_DEL_YN",
////                "type"=>"checkbox",
////                "default"=>"Y",
////                "text"=>"Delete"
////            ]
//                ],
//        ],
//    ];
    protected $preview_url;

    protected $search_fields;//used only when this controller has index page.(i.e $this->has_index_page = true)

    /* table_fields
     * label: field label description
     * name: db field name
     * align: cell align type [left|center|right]
     * type: field type
     */
    protected $table_fields;

    //Publish settings
//    protected $form_description="Register and set the header image and footer image to be displayed on the top page.";
    //Success settings
//    protected $success_return_text = "Return";

    public function __construct()
    {
        parent::__construct();

        $this->login_check();
    }

    protected function initialize() {
        $this->page_title = lang('headerfooter_design_setting');//"Design setting";
        $this->page_subtitle = lang('headerfooter_image_edit');//"header footer image / edit";
        $this->panel_title = lang('headerfooter_header_footer_image_settings');//"Header/footer image settings";
        $this->fields = [
            [
                "group"=>true,
                "label"=>lang('headerfooter_header_image'),//"Header image(jpeg,jpg,gif,png)",
                "input"=>[[
                    "name"=>"HEADER_IMAGE_DEL_YN",
                    "type"=>"checkbox",
                    "checked"=>"Y",
                    "unchecked"=>"N",
                    "text"=>lang('delete')
                ],
                    [
                    "name"=>"HEADER_IMAGE",
                    "type"=>"file",
                    "help"=>"<span class='help-tab-gray'>Format</span> jpeg,jpg,gif,png</br>
                         <span class='help-tab-gray'>Recommended size</span>640*84</br>
                         <span class='help-tab-gray'>capacity</span>Please register the image within 300KB</br>
                         <span class='help-tab-gray'>File name</span>Alphanumeric file name
                         ",
                ],

                ],
            ],
            [
                "group"=>true,
                "label"=>lang('headerfooter_footer_image'),//"Footer image(jpeg,jpg,gif,png)",
                "input"=>[
                    [
                        "name"=>"FOOTER_IMAGE_DEL_YN",
                        "type"=>"checkbox",
                        "checked"=>"Y",
                        "unchecked"=>"N",
                        "text"=>lang('delete')
                    ],
                    [
                    "name"=>"FOOTER_IMAGE",
                    "type"=>"file",
                    "help"=>"<span class='help-tab-gray'>Format</span> jpeg,jpg,gif,png</br>
                         <span class='help-tab-gray'>Recommended size</span>640*100</br>
                         <span class='help-tab-gray'>capacity</span>Please register the image of less than 300KB</br>
                         <span class='help-tab-gray'>File name</span>Alphanumeric characters in the file name
                         ",
                ],
                ],
            ],
        ];

        ///////////confirm  fields
        $this->confirm_fields = [
            [
                "group"=>true,
                "label"=>lang('confirm_headerfooter_header_image'),//"Header image",
                "input"=>[[
                    "name"=>"HEADER_IMAGE_DEL_YN",
                    "type"=>"checkbox",
                    "checked"=>"Y",
                    "unchecked"=>"N",
                    "text"=>lang('confirm_delete')
                ],
                    [
                    "name"=>"HEADER_IMAGE",
                    "type"=>"file",
                    "help"=>lang('confirm_header_image_help')//"<span class='help-tab-gray'>Format</span> jpeg,jpg,gif,png</br>
                         //<span class='help-tab-gray'>Recommended size</span>640*84</br>
                         //<span class='help-tab-gray'>capacity</span>Please register the image within 300KB</br>
                         //<span class='help-tab-gray'>File name</span>Alphanumeric file name
                         //",
                ],

                ],
            ],
            [
                "group"=>true,
                "label"=>lang('confirm_headerfooter_footer_image'),//"Footer image",
                "input"=>[
                    [
                        "name"=>"FOOTER_IMAGE_DEL_YN",
                        "type"=>"checkbox",
                        "checked"=>"Y",
                        "unchecked"=>"N",
                        "text"=>lang('confirm_delete')
                    ],
                    [
                    "name"=>"FOOTER_IMAGE",
                    "type"=>"file",
                    "help"=>lang('confirm_footer_image_help')//"<span class='help-tab-gray'>Format</span> jpeg,jpg,gif,png</br>
                         //<span class='help-tab-gray'>Recommended size</span>640*100</br>
                         //<span class='help-tab-gray'>capacity</span>Please register the image of less than 300KB</br>
                         //<span class='help-tab-gray'>File name</span>Alphanumeric characters in the file name
                         
                ],
                ],
            ],
        ];

        $this->form_description=lang('headerfooter_register');//"Register and set the header image and footer image to be displayed on the top page.";
        //Success settings
        $this->success_return_text = lang('headerfooter_return');//"Return";

        $this->load->model('Headerfooter_model', 'model');
        $this->head['title'] = "Design Settings";
        $this->head["description"] = "";

        $this->preview_url = site_url("preview/main/headerandfooter");
    }
}
