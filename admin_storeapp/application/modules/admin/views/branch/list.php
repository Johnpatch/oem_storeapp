
<link rel="stylesheet" href="<?=base_url('assets/additional/icons/icomoon/styles.css')?>">
<link rel="stylesheet" href="<?=base_url('assets/additional/icons/fontawesome/styles.min.css')?>">
<!-- <link rel="stylesheet" href="{{ asset('assets/plugins/additional/core.css') }}"> -->
<link rel="stylesheet" href="<?=base_url('assets/additional/components.css')?>">
<link rel="stylesheet" href="<?=base_url('assets/additional/colors.css') ?>">

<script type="text/javascript" src="<?=base_url('assets/additional/tables/datatables/datatables.min.js')?>"></script>
<script type="text/javascript" src="<?=base_url('assets/additional/tables/datatables/extensions/fixed_columns.min.js')?>"></script>
<script type="text/javascript" src="<?=base_url('assets/additional/tables/datatables/extensions/col_reorder.min.js')?>"></script>
<script type="text/javascript" src="<?=base_url('assets/additional/tables/datatables/extensions/buttons.min.js')?>"></script>
<script type="text/javascript" src="<?=base_url('assets/additional/forms/selects/bootstrap_select.min.js')?>"></script>
<script type="text/javascript" src="<?=base_url('assets/additional/forms/selects/select2.full.min.js')?>"></script>

<div class="content-wrapper">
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <h4><span class="page-maintitle"><?= lang('branch_list')?></span></h4>
            </div>
        </div>
        <div class="cms-breadcrumb">
            <div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
                <ul class="breadcrumb">
                    <li><a href="<?= base_url('/').'?token='.$token ?>" class="breadcrumb-1"><?= lang('home') ?></a>&nbsp;&nbsp;&nbsp;<i
                            class="fa fa-angle-right breadcrumb-size"></i></li>
                    <li><a href="#" class="breadcrumb-1"></a><?= lang("branch_list")?></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="panel">
        <div class="panel-heading"></div>
        <div class="panel-body" style="padding-top:10px;">
            <div class="row">
                <div class="col-md-12">
                    <p class="content-group">
                    </p>
                    <div class="row b-margin-20">
                        <div class="col-md-9">
                            <span class="input-group-btn">
                                <a href="<?=base_url("admin/branch/register").'?token='.$token?>" class="btn create-btn">
                                    <?= lang('branch_register') ?>
                                </a>
                            </span>
                        </div>
                        <div class="col-md-3">
                            <div class="input-group">
                                <input type="text" class="form-control" name="search_key" placeholder="<?= lang("search_keyword")?>">
                                <span class="input-group-btn">
                                    <button id="btn_search" class="btn bg-search" type="button"><?= lang("search")?></button>
                                </span>
                            </div>
                        </div>
                    </div>
                    <!--table @test start-->
                    <div class="table-responsive clear-both">
                        <table id="branch_list" class="table table-striped table-bordered" style="width: 100%;">

                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>


    var oTable;
    var table;
    function remove(id) {
        var params = {
            'id' : id
        };
        bootbox.confirm({
            message: "<?= lang("delete_confirm")?>",
            buttons: {
                confirm: {
                    label: "<?= lang("yes")?>",
                    className: 'btn-success'
                },
                cancel: {
                    label: "<?= lang("no")?>",
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
                if (result) {
                    $.post("<?=base_url('admin/branch/remove').'?token='.$token?>", params, function(data, status){
                        data = JSON.parse(data);
                        if (data.success) {
                            data_reload();
                        }
                    });
                }
            }
        });
    }

    function data_reload() {
        oTable.api().ajax.url(oTable.fnSettings().sAjaxSource).load();
    }

    $(function() {
        $.extend( $.fn.dataTable.defaults, {
            autoWidth: false,
            colReorder: true,
            dom: '<<t><"pagination-nav"pl>>',   // filter
            language: {
                lengthMenu: '_MENU_',
                paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
            },
            drawCallback: function () {
                $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').addClass('dropup');
            },
            preDrawCallback: function() {
                $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
            }
        });

        // Generate content for a column
        table = $('#branch_list').DataTable({
            "paging": true,    // pagination nav
            "info": false,      // Showing 1 to 17 of 17 entries
            "searching": false,
            "bSortable": true,
            "sort": true,
            "bServerSide": true,
            "bProcessing": true,
            "aoColumns": [
                {
                    "sTitle" : "", "mData": "","sWidth": 50, "class":"text-center",
                    mRender: function (data, type, row, pos) {
                        var info = table.page.info();
                        var result = Number(info.page) * Number(info.length) + Number(pos.row) + 1;
                        return result;
                    }
                },
                { "sTitle" : "<?=lang("login_id")?>", "mData": "LOGIN_ID", "class":"text-center"},
                { "sTitle" : "<?=lang("name")?>", "mData": "NAME", "class":"text-center"},
                { "sTitle" : "<?=lang("google_play_url")?>", "mData": "GOOGLE_PLAY_URL", "class":"text-center"},
                { "sTitle" : "<?=lang("app_store_url")?>", "mData": "APP_STORE_URL", "class":"text-center"},
                { "sTitle" : "<?=lang("create_time")?>", "mData": "CREATE_TIME", "class":"text-center"},
                {
                    "sTitle" : "<?=lang("manage")?>", "mData": "CREATE_TIME", "class":"text-center",
                    mRender: function (data, type, row, pos) {
                        return  '<a class="btn edit-btn" href="<?=base_url('admin/branch/register')?>/' + row.ID + '?token=<?= $token ?>' + '">' +
                            '<?= lang("edit")?>' +
                            '</a>' +
                            '<a class="btn delete-btn" onclick="remove(\'' + row.ID + '\');">' +
                            '<?= lang("delete")?>' +
                            '</a>';
                    }
                }
            ],
            "bAutoWidth": true,
            "sAjaxSource": "<?=base_url('/admin/branch/read').'?token='.$token?>",
            "fnServerParams": function (aoData) {
                aoData.push(
                    { "name" : "search",    "value" : $('input[name="search_key"]').val() },
                    // { "name" : "is_price",    "value" : $('#is_price').val() },
                    // { "name" : "currency",    "value" : $('#currency').val() },
                    // { "name" : "register",    "value" : $('#register').val() }
                );
                return aoData;
            },
            "sAjaxDataProp": "list",
            scrollX: true,
            scrollCollapse: true,
            // "order": [
            //     [0, "asc"]
            // ],
            lengthMenu: [[10, 25, 50, -1], ["10", "25", "50", "All"]],
            "ideferLoading": 1,
            "bDeferRender": true,
            buttons: {
                buttons: [
                ]
            },
            columnDefs: [{
                bSortable: false,
                targets: [0, 2, 3, 4, 5]
            }],
            
            initComplete: function () {
                oTable = this;
            }
        });

        setTimeout(function() {
            $(window).on('resize', function () {
                table.columns.adjust();
            });
        }, 100);

        $("#btn_search").click(function() {
            data_reload();
        });
    });

</script>