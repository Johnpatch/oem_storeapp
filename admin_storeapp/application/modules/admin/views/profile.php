<style>
    #message {
        margin-top: 10px;
        padding: 5px;
    }
</style>
<div class="content-wrapper">
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <h4><span class="page-maintitle"><?= lang('profile')?></span></h4>
            </div>
        </div>
        <div class="cms-breadcrumb">
            <div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
                <ul class="breadcrumb">
                    <li><a href="<?= base_url('/').'?token='.$token ?>" class="breadcrumb-1"><?= lang('home') ?></a>&nbsp;&nbsp;&nbsp;<i
                            class="fa fa-angle-right breadcrumb-size"></i></li>
                    <li><a href="#" class="breadcrumb-1"></a><?= lang("profile")?></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="panel">
        <?php
          //  $user = $this->session->userdata('user');
        ?>
        <div class="panel-body" style="display: block;">
            <form class="form-horizontal" action="profile/submit?token=<?= $token ?>" method="post" id="submit_form" enctype="multipart/form-data">
                <div class="form-group">
                    <label class="control-label col-lg-2"><?=lang('login_id')?></label>
                    <div class="col-lg-10">
                        <input type="text" class="form-control" value="<?=$user->USERID?>" readonly>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-lg-2"><?=lang('password')?></label>
                    <div class="col-lg-10">
                        <input type="password" class="form-control" name="PASSWORD" id="password">
                        <div class="alert alert-danger hidden" id="message"></div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-lg-2"><?=lang('confirm_password')?></label>
                    <div class="col-lg-10">
                        <input type="password" class="form-control" value="" id="confirm">
                    </div>
                </div>
		<div class="form-group">
                    <label class="control-label col-lg-2"><?=lang('logo')?></label>
                    <div class="col-lg-10">
                        <button type="button" class="btn btn-success" id="choose">画像選択</button>
                        <div class="row">
                            <div class="col-md-12" style="margin-top: 20px;">
                                <img src="<?=$setting && $setting->LOGO ? $setting->LOGO : ''?>" id="preview" style="height: 150px;">
                            </div>
                        </div>
                    </div>
                </div>
		<div class="form-group">
                    <label class="control-label col-lg-2">PDF</label>
                    <div class="col-lg-10">
                        <button type="button" class="btn btn-success" id="choose1">PDFの選択</button>
			<div class="row">
                            <div class="col-md-12" style="margin-top: 20px;" id="pdf_filename">
                                
                            </div>
                        </div>
                    </div>
                </div>
                <div class="hidden">
                    <input type="text" name="ID" value="<?=$user->ID?>">
                    <input type="text" name="TYPE" value="<?=$user->TYPE?>">
		    <input type="text" name="LOGO" value="">
                    <input type="file" id="file">
		    <input type="file" id="file1" name="pdf_file">
                </div>
                <div class="text-right">
                    <button class="btn btn-primary"><?=lang("update")?> <i class="icon-arrow-right14 position-right"></i></button>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    $(function() {
        $('#submit_form').submit(function() {
            var password = $('#password').val();
            var confirm = $('#confirm').val();
	    if(password || confirm){
            if (!password) {
                warning('<?=lang('message_empty')?>');
                return false;
            } else if (password != confirm) {
                warning('<?=lang('message_match')?>');
                return false;
            } else if (password.length < 6) {
                warning('<?=lang('message_length')?>');
                return false;
            }}
        });
	$('#choose').click(function() {
            $('#file').click();
        });
	$('#choose1').click(function() {
            $('#file1').click();
        });

        $('#file').change(function() {
            preview(this);
        });
	$('#file1').change(function() {
            preview1(this);
        });

    });
    function warning(message) {
        console.log(message);
        $('#message').html(message);
        $('#message').removeClass('hidden');
    }
       function preview(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                var base64 = e.target.result;
                $('#preview').attr('src', base64);
                $('input[name="LOGO"]').val(base64);
                $('#preview').removeClass();
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
	function preview1(input) {
		$('#pdf_filename').html(input.files[0]['name']);
	}
</script>