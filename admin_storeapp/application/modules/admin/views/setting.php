<style>
    table tr>td:first-child {
        width: 50px;
    }
</style>
<div class="content-wrapper">
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <h4><span class="page-maintitle"><?= lang('control_panel')?></span></h4>
            </div>
        </div>
        <div class="cms-breadcrumb">
            <div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
                <ul class="breadcrumb">
                    <li><a href="<?= base_url('/').'?token='.$token ?>" class="breadcrumb-1"><?= lang('home') ?></a>&nbsp;&nbsp;&nbsp;<i
                            class="fa fa-angle-right breadcrumb-size"></i></li>
                    <li><a href="#" class="breadcrumb-1"></a><?= lang("control_panel")?></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="panel">
        <div class="panel-body" style="display: block;">
            <div class="col-lg-12">
                <div class="row">
                    <form action="<?=base_url('admin/setting/submit').'?token='.$token?>" id="layout_form" method="post">
                        <div class="table-responsive b-margin-20">
                            <table class="table table-bordered" style="background-color: #FFF;">
                                <tbody id="form-body">
                                    <tr>
                                        <td><?= lang("logo")?></td>
                                        <td>
                                            <div class="row">
                                                <div class="col-lg-4">
                                                    <button type="button" class="btn btn-success" id="choose">Choose</button>
                                                    <div class="row">
                                                        <div class="col-md-12" style="margin-top: 20px;">
                                                            <img src="<?=$setting && $setting['LOGO'] ? $setting['LOGO'] : ''?>" id="preview" style="height: 150px;">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><?=lang("copyright")?></td>
                                        <td>
                                            <textarea name="COPYRIGHT" class="form-control" rows="10" style="resize:none;"><?=$setting && $setting['COPYRIGHT'] ? $setting['COPYRIGHT'] : ''?></textarea>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="text-center">
                            <button class="btn common-btn-green-small custom-btn">
                                <?= lang('save') ?></button>
                        </div>
                        <div class="hidden">
                            <input type="text" name="ID" value="<?=$setting && $setting['ID'] ? $setting['ID'] : ''?>">
                            <input type="text" name="LOGO" value="">
                            <input type="file" id="file">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>

    function preview(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                var base64 = e.target.result;
                $('#preview').attr('src', base64);
                $('input[name="LOGO"]').val(base64);
                console.log(base64);
                $('#preview').removeClass();
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    $(function (){
        $('#choose').click(function() {
            $('#file').click();
        });

        $('#file').change(function() {
            preview(this);
        });
    });
</script>